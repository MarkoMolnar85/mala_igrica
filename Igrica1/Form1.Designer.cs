﻿namespace Igrica1
{
    partial class formaZaIgru
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelZaIgru = new System.Windows.Forms.Panel();
            this.imgLoptica = new System.Windows.Forms.PictureBox();
            this.imgUdarac = new System.Windows.Forms.PictureBox();
            this.timBrojacVremena = new System.Windows.Forms.Timer(this.components);
            this.labelRezultat = new System.Windows.Forms.Label();
            this.panelZaIgru.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgLoptica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgUdarac)).BeginInit();
            this.SuspendLayout();
            // 
            // panelZaIgru
            // 
            this.panelZaIgru.BackColor = System.Drawing.Color.Silver;
            this.panelZaIgru.Controls.Add(this.labelRezultat);
            this.panelZaIgru.Controls.Add(this.imgLoptica);
            this.panelZaIgru.Controls.Add(this.imgUdarac);
            this.panelZaIgru.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelZaIgru.Location = new System.Drawing.Point(0, 0);
            this.panelZaIgru.Name = "panelZaIgru";
            this.panelZaIgru.Size = new System.Drawing.Size(579, 445);
            this.panelZaIgru.TabIndex = 0;
            // 
            // imgLoptica
            // 
            this.imgLoptica.BackColor = System.Drawing.Color.DarkRed;
            this.imgLoptica.Location = new System.Drawing.Point(235, 59);
            this.imgLoptica.Name = "imgLoptica";
            this.imgLoptica.Size = new System.Drawing.Size(30, 30);
            this.imgLoptica.TabIndex = 1;
            this.imgLoptica.TabStop = false;
            // 
            // imgUdarac
            // 
            this.imgUdarac.BackColor = System.Drawing.Color.Black;
            this.imgUdarac.Location = new System.Drawing.Point(128, 398);
            this.imgUdarac.Name = "imgUdarac";
            this.imgUdarac.Size = new System.Drawing.Size(300, 30);
            this.imgUdarac.TabIndex = 0;
            this.imgUdarac.TabStop = false;
            // 
            // timBrojacVremena
            // 
            this.timBrojacVremena.Interval = 3;
            this.timBrojacVremena.Tick += new System.EventHandler(this.timBrojacVremena_Tick);
            // 
            // labelRezultat
            // 
            this.labelRezultat.AutoSize = true;
            this.labelRezultat.Font = new System.Drawing.Font("Microsoft YaHei UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRezultat.Location = new System.Drawing.Point(360, 75);
            this.labelRezultat.Name = "labelRezultat";
            this.labelRezultat.Size = new System.Drawing.Size(80, 14);
            this.labelRezultat.TabIndex = 2;
            this.labelRezultat.Text = "BrojBodova: 0";
            // 
            // formaZaIgru
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 445);
            this.Controls.Add(this.panelZaIgru);
            this.Name = "formaZaIgru";
            this.Text = "Form1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TasterDole);
            this.panelZaIgru.ResumeLayout(false);
            this.panelZaIgru.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgLoptica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgUdarac)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelZaIgru;
        private System.Windows.Forms.PictureBox imgLoptica;
        private System.Windows.Forms.PictureBox imgUdarac;
        private System.Windows.Forms.Timer timBrojacVremena;
        private System.Windows.Forms.Label labelRezultat;
    }
}

