﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Igrica1
{
    public partial class formaZaIgru : Form
    {
        int kretanjePoX = 4;
        int kretanjePoY = 4;
        int brojBodova = 0;
        public formaZaIgru()
        {
            InitializeComponent();
            timBrojacVremena.Enabled = true;
            this.FormBorderStyle = FormBorderStyle.None;
            this.Bounds = Screen.PrimaryScreen.Bounds;
            Cursor.Hide();
            imgUdarac.Location = new Point(panelZaIgru.Right / 2 - imgUdarac.Width / 2, panelZaIgru.Bottom - imgUdarac.Height - 5);
        }

        private void TasterDole(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
            if (e.KeyCode == Keys.P && timBrojacVremena.Enabled == true)
            {
                timBrojacVremena.Enabled = false; 
            }
            else if (e.KeyCode == Keys.P && timBrojacVremena.Enabled == false)
            {
                timBrojacVremena.Enabled = true;
            }
        }

        private void timBrojacVremena_Tick(object sender, EventArgs e)
        {
            imgUdarac.Left = Cursor.Position.X - imgUdarac.Width / 2;
            imgLoptica.Top = imgLoptica.Top + kretanjePoY;
            imgLoptica.Left += kretanjePoX;
            if (imgLoptica.Bottom >= imgUdarac.Top && imgLoptica.Right >= imgUdarac.Left && imgLoptica.Left <= imgUdarac.Right && imgLoptica.Right <= imgUdarac.Width / 2 + imgUdarac.Left)
            {
                brojBodova++;
                kretanjePoY = -kretanjePoY;
                if (kretanjePoX > 0)
                {
                    kretanjePoX = -kretanjePoX;
                }
                else
                { }
                labelRezultat.Text = "Broj bodova: " + brojBodova;
                if (kretanjePoX > 0)
                {
                    kretanjePoX += 1;
                }
                else
                    kretanjePoX -= 1;
                if (kretanjePoY > 0)
                {
                    kretanjePoY += 1;
                }
                else
                    kretanjePoY -= 1;
            }
            if (imgLoptica.Bottom >= imgUdarac.Top && imgLoptica.Right >= imgUdarac.Left && imgLoptica.Left <= imgUdarac.Right && imgLoptica.Left >= imgUdarac.Width / 2 + imgUdarac.Left)
            {
                brojBodova++;
                kretanjePoY = -kretanjePoY;
                if (kretanjePoX < 0)
                {
                    kretanjePoX = -kretanjePoX;
                }
                else
                { }
                labelRezultat.Text = "Broj bodova: " + brojBodova;
                if (kretanjePoX > 0)
                {
                    kretanjePoX += 1;
                }
                else
                    kretanjePoX -= 1;
                if (kretanjePoY > 0)
                {
                    kretanjePoY += 1;
                }
                else
                    kretanjePoY -= 1;
            }
            if (imgLoptica.Right >= panelZaIgru.Right)
            {
                kretanjePoX = -kretanjePoX;
            }
            if (imgLoptica.Left <= panelZaIgru.Left)
            {
                kretanjePoX = -kretanjePoX;
            }
            if (imgLoptica.Top <= panelZaIgru.Top)
            {
                kretanjePoY = -kretanjePoY;
            }

            
            if(imgLoptica.Bottom >=  panelZaIgru.Bottom)
            {
                timBrojacVremena.Enabled = false;
            }
        }
        static void Ubrzanje()
        {
 
        }
    }
}
